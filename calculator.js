const FEBRUARY=1;
const APRIL=3;
const JUNE=5;
const SEPTEMBER=8;
const NOVEMBER=10;

const YEAR_DROP_DOWN_RANGE=100;

const MAX_DAYS_PER_MONTH = 31;
const NORMAL_DAYS_PER_MONTH = 30;
const DAYS_PER_LEAP_FEBRUARY = 29;
const DAYS_PER_NONLEAP_FEBRUARY = 28;

const MONTHS_IN_YEAR = 12;

const DAYS_PER_YEAR = 365.25;
const MILISECONDS_PER_DAY = 86400000;

const ALPHA_REGEX = /^[ A-Za-z]+$/;


let date = new Date();
let yearNow = date.getFullYear();
let monthNow = date.getMonth();
let dayNow = date.getDate();

function getAge(){
	let year = parseInt(document.getElementById("year").value);
	let month = parseInt(document.getElementById("month").value);
	let day = parseInt(document.getElementById("day").value);
		
	let name= document.getElementById("name").value;
	
	if(isNaN(year)||isNaN(month)||isNaN(day)){
		document.getElementById("error").innerHTML="DateError: Please reenter your birthday";
		document.getElementById("answer").innerHTML = "";
	}
	else if(name==""){
		document.getElementById("error").innerHTML="NameError: Please enter your name";
		document.getElementById("answer").innerHTML = ""
	}
	else if(validateName(name)){
		document.getElementById("error").innerHTML="NameError: You can enter only alpha characters";
		document.getElementById("answer").innerHTML = "";
	}
	else{		
		document.getElementById("error").innerHTML="";
		let birthdate=new Date(year,month,day);
		
		let age_diff=Date.now()-birthdate;
		
		let age= age_diff / ( MILISECONDS_PER_DAY * DAYS_PER_YEAR );
		let age_years= Math.floor(age);
		let age_months= Math.floor((age - age_years) * MONTHS_IN_YEAR)
		let age_days= (dayNow >= day) ? dayNow - day : NORMAL_DAYS_PER_MONTH + (dayNow - day);
		
		document.getElementById("answer").innerHTML = "Name: "+name+ "<br> Age: "+age_years+" years "+age_months+" months "+ age_days+" days ";
	}
}


function validateName(name){
	return name.match(ALPHA_REGEX) == null;
}

	
function populateYearDropDown(){
	for( let year = yearNow ; year > yearNow - YEAR_DROP_DOWN_RANGE ; year--){
		let x = document.getElementById("year");
		let option = document.createElement("option");
		option.text = year;
		x.add(option);
	}
}

function enableMonths(){
	
	let selectedYear=parseInt(document.getElementById("year").value);
	let endMonth=MONTHS_IN_YEAR;
	
	if(selectedYear===yearNow){
		endMonth=monthNow+1;
	}
	
	for(let i = 1 ; i <= endMonth ; i++){
		document.getElementById("month").options[i].disabled=false;
	}
	
	for(let i = endMonth + 1; i <= MONTHS_IN_YEAR; i++){
		document.getElementById("month").options[i].disabled=true;
	}
}

function enableDays(){
	
	let selectedMonth=parseInt(document.getElementById("month").value);
	let selectedYear = parseInt(document.getElementById("year").value);	

	let thirtyDayMonths = [APRIL, JUNE, SEPTEMBER, NOVEMBER];
	let endDay;
	
	if( selectedMonth === FEBRUARY ){
		/*
		To be a leap year it should either be	*divisible by 4 and not divisible by 100 
												OR
												*should be divisible by 400
		*/
		if( ( ( selectedYear % 4 === 0 ) &&  (selectedYear % 100 !== 0 ) ) || ( selectedYear % 400 === 0 ) ){
			
			/*Days 30 and 31 are hidden*/
			document.getElementById("day").options[31].hidden=true;
			document.getElementById("day").options[30].hidden=true;
			endDay=DAYS_PER_LEAP_FEBRUARY;
		}
		else{
			/*Days 29, 30 and 31 are hidden*/
			document.getElementById("day").options[31].hidden=true;
			document.getElementById("day").options[30].hidden=true;
			document.getElementById("day").options[29].hidden=true;
			endDay=DAYS_PER_NONLEAP_FEBRUARY;
		}
	}
	else if(thirtyDayMonths.includes(selectedMonth)){
		endDay=NORMAL_DAYS_PER_MONTH;
		/*Day 31 is hidden*/
		document.getElementById("day").options[29].hidden=false;
		document.getElementById("day").options[30].hidden=false;
		document.getElementById("day").options[31].hidden=true;
	
	}else{
		endDay=MAX_DAYS_PER_MONTH;
		
		/*Unhide Days 29, 30 and 31*/
		document.getElementById("day").options[29].hidden=false;
		document.getElementById("day").options[30].hidden=false;
		document.getElementById("day").options[31].hidden=false;
	}
	
	
	if( (selectedMonth ==  monthNow) && (selectedYear == yearNow) ){
		endDay= dayNow;
	}
	
	for( let daySelect = 1 ; daySelect <= endDay ; daySelect++){
		document.getElementById("day").options[daySelect].disabled=false;
	}
	for( let daySelect = endDay + 1 ; daySelect <= MAX_DAYS_PER_MONTH ; daySelect++){
		document.getElementById("day").options[daySelect].disabled=true;
	}
	
}

document.addEventListener('readystatechange', event => { 
    if (event.target.readyState === "interactive") {
		populateYearDropDown();
    }
});